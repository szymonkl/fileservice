﻿using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;

namespace FileService.ApiTests
{
    public class ApiTestsFileRepository : BaseRepository<FileEntity>, IFileRepository
    {
        public ApiTestsFileRepository(ICloudTableProvider provider, string tableName, ILogger<BaseRepository<FileEntity>> logger) : base(provider, tableName, logger)
        {
        }
        public ApiTestsFileRepository(ICloudTableProvider provider, ILogger<BaseRepository<FileEntity>> logger) : base(provider, "apiTestsFilesMetadata", logger)
        {
        }
    }
}
