﻿using FileService.Persistence.Blobs;
using Microsoft.Extensions.Logging;

namespace FileService.ApiTests
{
    public class ApiTestsFileStorage : BaseStorage, IFileStorage
    {
        public ApiTestsFileStorage(IAzureStorageContainerProvider containerProvider, string containerName, ILogger<BaseStorage> logger) : base(containerProvider, "apitestsfilestorage", logger)
        {
            // For testing purposes
        }
        public ApiTestsFileStorage(IAzureStorageContainerProvider containerProvider, ILogger<BaseStorage> logger) : base(containerProvider, "apitestsfilestorage", logger)
        {
            // For application purposes
        }
    }
}
