﻿using FileService.Common.Helpers;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace FileService.ApiTests
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>, IAsyncLifetime
    {
        public string FilePath { get; private set; }
        public string FileName { get; private set; }

        protected override IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(builder =>
                {
                    builder.UseStartup<Startup>();
                    builder.ConfigureTestServices(s =>
                    {
                        s.Add(new ServiceDescriptor(typeof(IFileRepository),
                            typeof(ApiTestsFileRepository), ServiceLifetime.Scoped));
                        s.Add(new ServiceDescriptor(typeof(IFileStorage), typeof(ApiTestsFileStorage),
                            ServiceLifetime.Scoped));
                    });
                })
                .ConfigureAppConfiguration((context, config) => config.AddJsonFile("appsettings.Development.json"));

        }

        public Task InitializeAsync()
        {
            return Task.Run(() =>
            {
                FileName = StringHelper.GetRandomString(6);
                FilePath = Path.Combine(Path.GetTempPath(), $"{StringHelper.GetRandomString(6)}.txt");
            });
        }

        public Task DisposeAsync()
        {
            return Task.Run(() => FileHelper.DeleteFile(FilePath));
        }
    }
}
