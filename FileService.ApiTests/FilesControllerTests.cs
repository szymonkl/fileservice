using FileService.Application.Models;
using FileService.Common.Helpers;
using FluentAssertions;
using Microsoft.AspNet.OData;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions.Ordering;

namespace FileService.ApiTests
{
    [Collection("Sequential")]
    public class FilesControllerTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;
        private readonly string _fileName;
        private readonly string _filePath;
        private const string BaseEndpoint = "/api/Files";
        private const string ApiVersion = "api-version=1.0";
        private const string FileType = "firmware";
        private const string Language = "English";
        private const string Version = "1.0.0.0";

        private static readonly string UpdatedFileName = StringHelper.GetRandomString(6);
        private const string UpdatedType = "tts_voice_announcement";
        private const string UpdatedExtension = ".wav";
        private const string UpdatedVersion = "2.0.0.0";

        public FilesControllerTests(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _filePath = factory.FilePath;
            _fileName = factory.FileName;
        }

        [Fact, Order(1)]
        public async Task PostAsync_WhenValidRequest_FileUploaded()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            FileHelper.CreateFile(_filePath);
            var request = new UploadFileRequest
            {
                Target = "amp",
                Name = _fileName,
                LocalPath = _filePath,
                Type = FileType,
                CustomMetadata = new Dictionary<string, object>
                {
                    ["language"] = Language,
                    ["version"] = Version,
                    ["uniqueKey"] = _fileName
                }
            };

            var response = await _client.PostAsJsonAsync(endpoint, request);

            response.StatusCode.Should().Be(HttpStatusCode.Created);
            var result = await response.Content.ReadAsAsync<FileMetadata>();
            result.Should().NotBeNull();
            result.Name.Should().Be(_fileName);
            result.ExistsOnBlob.Should().Be(true);
            result.UploadingStatus.Should().Be(UploadingStatus.Completed);
        }

        [Fact, Order(2)]
        public async Task GetAll_AllFilesReturned()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<FileMetadata>>>();
            result.Value.Should().HaveCountGreaterThan(0);
            result.Value.Should().AllBeOfType<FileMetadata>();
        }

        [Fact, Order(3)]
        public async Task GetAll_FilterByName_ResultShouldBeFiltered()
        {
            var endpoint = $"{BaseEndpoint}?$filter=name eq '{_fileName}'&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<FileMetadata>>>();
            result.Value.Should().HaveCount(1);
            result.Value.Should().AllBeOfType<FileMetadata>();
        }

        [Fact, Order(4)]
        public async Task GetAll_FilterByCustomMetadata_ResultShouldBeFiltered()
        {
            var endpoint = $"{BaseEndpoint}?$filter=uniqueKey eq '{_fileName}'&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<FileMetadata>>>();
            result.Value.Should().HaveCount(1);
            result.Value.Should().AllBeOfType<FileMetadata>();
        }

        [Fact, Order(5)]
        public async Task GetAll_SelectUrls_UrlsReturned()
        {
            var endpoint = $"{BaseEndpoint}?$select=url&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<FileMetadata>>>();
            result.Value.Should().HaveCountGreaterThan(0);
            result.Value.Should().AllBeOfType<FileMetadata>();
            result.Value.Should().OnlyContain(x => x.Name == null);
            result.Value.Should().OnlyContain(x => x.Url != null);
            result.Value.Should().OnlyContain(x => x.Url.Contains("http"));
        }

        [Fact, Order(6)]
        public async Task GetAll_SelectCustomMetadata_MetadataReturned()
        {
            var endpoint = $"{BaseEndpoint}?$select=uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<Dictionary<string, object>>>>();
            result.Value.Should().HaveCountGreaterThan(0);
            result.Value.Should().AllBeOfType<Dictionary<string, object>>();
            result.Value.Should().OnlyContain(x => !x.ContainsKey("name"));
            result.Value.Should().Contain(x => x["uniqueKey"].As<string>().Contains(_fileName));
        }

        [Fact, Order(7)]
        public async Task GetAll_SelectManyProperties_PropertiesReturned()
        {
            var endpoint = $"{BaseEndpoint}?$select=name,uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<ODataValue<IEnumerable<Dictionary<string, object>>>>();
            result.Value.Should().HaveCountGreaterThan(0);
            result.Value.Should().AllBeOfType<Dictionary<string, object>>();
            result.Value.Should().Contain(x => x["name"].As<string>().Equals(_fileName));
            result.Value.Should().Contain(x => x["uniqueKey"].As<string>().Contains(_fileName));
        }

        [Fact, Order(8)]
        public async Task Get_FileReturned()
        {
            var endpoint = $"{BaseEndpoint}/{FileType}-{_fileName}?{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();
            result["name"].Should().Be(_fileName);
            result["extension"].Should().Be(Path.GetExtension(_filePath));
            result["target"].Should().Be("amp");
            result["type"].Should().Be(FileType);
            result["url"].As<string>().Should().ContainAny("http", "https").And.Contain(_fileName);
            result["existsOnBlob"].As<bool>().Should().BeTrue();
            result["uploadingStatus"].Should().Be("Completed");
            result["language"].Should().Be(Language);
            result["version"].Should().Be(Version);
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(9)]
        public async Task Get_SelectUrl_UrlReturned()
        {
            var endpoint = $"{BaseEndpoint}/{FileType}-{_fileName}?$select=url&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<FileMetadata>();
            result.Url.Should().ContainAny("http", "https").And.Contain(_fileName);
            result.Name.Should().BeNull();
        }

        [Fact, Order(10)]
        public async Task Get_SelectCustomMetadata_CustomMetadataReturned()
        {
            var endpoint = $"{BaseEndpoint}/{FileType}-{_fileName}?$select=uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();

            result.Should().NotContainKey("name");
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(11)]
        public async Task Get_SelectManyProperties_ManyPropertiesReturned()
        {
            var endpoint = $"{BaseEndpoint}/{FileType}-{_fileName}?$select=name,uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();

            result["name"].Should().Be(_fileName);
            result["uniqueKey"].Should().Be(_fileName);
            result.Should().NotContainKey("url");
            result.Should().NotContainKey("version");
        }

        [Fact, Order(12)]
        public async Task GetByTypeByName_FileReturned()
        {
            var endpoint = $"{BaseEndpoint}/ByTypeByName(type='{FileType}',name='{_fileName}')?{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();

            result["name"].Should().Be(_fileName);
            result["extension"].Should().Be(Path.GetExtension(_filePath));
            result["target"].Should().Be("amp");
            result["type"].Should().Be(FileType);
            result["url"].As<string>().Should().ContainAny("http", "https").And.Contain(_fileName);
            result["existsOnBlob"].As<bool>().Should().BeTrue();
            result["language"].Should().Be(Language);
            result["version"].Should().Be(Version);
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(13)]
        public async Task GetByTypeByName_SelectUrl_UrlReturned()
        {
            var endpoint =
                $"{BaseEndpoint}/ByTypeByName(type='{FileType}',name='{_fileName}')?$select=url&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<FileMetadata>();

            result.Url.Should().ContainAny("http", "https").And.Contain(_fileName);
            result.Name.Should().BeNull();
        }

        [Fact, Order(14)]
        public async Task GetByTypeByName_SelectCustomMetadata_CustomMetadataReturned()
        {
            var endpoint =
                $"{BaseEndpoint}/ByTypeByName(type='{FileType}',name='{_fileName}')?$select=uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();

            result.Should().NotContainKey("name");
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(15)]
        public async Task GetByTypeByName_SelectManyProperties_ManyPropertiesReturned()
        {
            var endpoint = $"{BaseEndpoint}/ByTypeByName(type='{FileType}',name='{_fileName}')?$select=name,uniqueKey&{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await response.Content.ReadAsAsync<Dictionary<string, object>>();

            result["name"].Should().Be(_fileName);
            result["uniqueKey"].Should().Be(_fileName);
            result.Should().NotContainKey("url");
            result.Should().NotContainKey("version");
        }

        [Fact, Order(16)]
        public async Task Put_ValidMetadataSameKeys_MetadataUpdated()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            FileHelper.CreateFile(_filePath);
            var request = new UpdateFileMetadata
            {
                Name = _fileName,
                Type = FileType,
                Target = "mkz",
                Extension = UpdatedExtension,
                UploadingStatus = (UploadingStatus)2,
                CustomMetadata = new Dictionary<string, object>
                {
                    ["language"] = Language,
                    ["version"] = UpdatedVersion,
                    ["uniqueKey"] = _fileName
                }
            };

            var response = await _client.PutAsJsonAsync(endpoint, request);
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);

            var getEndpoint = $"{BaseEndpoint}/ByTypeByName(type='{FileType}',name='{_fileName}')?{ApiVersion}";
            var getResponse = await _client.GetAsync(getEndpoint);

            getResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await getResponse.Content.ReadAsAsync<Dictionary<string, object>>();

            result["name"].Should().Be(_fileName);
            result["extension"].Should().Be(UpdatedExtension);
            result["target"].Should().Be("mkz");
            result["type"].Should().Be(FileType);
            result["url"].As<string>().Should().ContainAny("http", "https").And.Contain(_fileName);
            result["existsOnBlob"].As<bool>().Should().BeTrue();
            result["uploadingStatus"].Should().Be("Failed");
            result["language"].Should().Be(Language);
            result["version"].Should().Be(UpdatedVersion);
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(17)]
        public async Task Put_ValidMetadataNewKeys_NewRecordAdded()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            FileHelper.CreateFile(_filePath);
            var request = new UpdateFileMetadata
            {
                Name = UpdatedFileName,
                Type = UpdatedType,
                Target = "mkz",
                Extension = UpdatedExtension,
                UploadingStatus = (UploadingStatus)2,
                CustomMetadata = new Dictionary<string, object>
                {
                    ["language"] = Language,
                    ["version"] = UpdatedVersion,
                    ["uniqueKey"] = _fileName
                }
            };

            var response = await _client.PutAsJsonAsync(endpoint, request);
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);

            var getEndpoint = $"{BaseEndpoint}/ByTypeByName(type='{UpdatedType}',name='{UpdatedFileName}')?{ApiVersion}";
            var getResponse = await _client.GetAsync(getEndpoint);

            getResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var result = await getResponse.Content.ReadAsAsync<Dictionary<string, object>>();

            result["name"].Should().Be(UpdatedFileName);
            result["extension"].Should().Be(UpdatedExtension);
            result["target"].Should().Be("mkz");
            result["type"].Should().Be(UpdatedType);
            result["url"].As<string>().Should().BeNull();
            result["existsOnBlob"].As<bool>().Should().BeFalse();
            result["uploadingStatus"].Should().Be("Failed");
            result["language"].Should().Be(Language);
            result["version"].Should().Be(UpdatedVersion);
            result["uniqueKey"].Should().Be(_fileName);
        }

        [Fact, Order(18)]
        public async Task Get_InvalidKey_BadRequest()
        {
            var endpoint = $"{BaseEndpoint}/{_fileName}?{ApiVersion}";
            var response = await _client.GetAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var result = await response.Content.ReadAsStringAsync();
            result.Should().Contain("Key");
        }

        [Fact, Order(19)]
        public async Task Post_InvalidMetadata_BadRequest()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            FileHelper.CreateFile(_filePath);
            var request = new UploadFileRequest
            {
                Target = string.Empty,
                Name = string.Empty,
                LocalPath = string.Empty,
                Type = string.Empty
            };

            var response = await _client.PostAsJsonAsync(endpoint, request);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var result = await response.Content.ReadAsStringAsync();
            result.Should().NotBeNull();
            result.Should().ContainAll("Target", "Name", "LocalPath", "Type");
        }

        [Fact, Order(20)]
        public async Task Put_InvalidMetadata_BadRequest()
        {
            var endpoint = $"{BaseEndpoint}?{ApiVersion}";
            FileHelper.CreateFile(_filePath);
            var request = new UpdateFileMetadata
            {
                Target = string.Empty,
                Name = string.Empty,
                Extension = string.Empty,
                UploadingStatus = (UploadingStatus)99,
                Type = string.Empty
            };

            var response = await _client.PutAsJsonAsync(endpoint, request);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var result = await response.Content.ReadAsStringAsync();
            result.Should().NotBeNull();
            result.Should().ContainAll("Target", "Name", "Extension", "UploadingStatus", "Type");
        }

        [Fact, Order(21)]
        public async Task Delete_InvalidKey_BadRequest()
        {
            var endpoint = $"{BaseEndpoint}/{_fileName}?{ApiVersion}";
            var response = await _client.DeleteAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var result = await response.Content.ReadAsStringAsync();
            result.Should().Contain("Key");
        }

        [Fact, Order(22)]
        public async Task Delete_EntityRemoved()
        {
            var endpoint = $"{BaseEndpoint}/{FileType}-{_fileName}?{ApiVersion}";
            var response = await _client.DeleteAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact, Order(23)]
        public async Task Delete_RecordAddedByPut_RecordRemoved()
        {
            var endpoint = $"{BaseEndpoint}/{UpdatedType}-{UpdatedFileName}?{ApiVersion}";
            var response = await _client.DeleteAsync(endpoint);

            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }
    }
}

