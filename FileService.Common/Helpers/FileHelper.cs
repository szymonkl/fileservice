﻿using System;
using System.IO;
using System.Text;

namespace FileService.Common.Helpers
{
    public static class FileHelper
    {
        public static bool CreateFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                using FileStream fs = File.Create(path);
                byte[] title = new UTF8Encoding(true).GetBytes("This is sample text file.");
                fs.Write(title, 0, title.Length);
                byte[] author = new UTF8Encoding(true).GetBytes("Test User");
                fs.Write(author, 0, author.Length);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
