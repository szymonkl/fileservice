﻿namespace FileService.Common.Helpers
{
    public static class TestHelper
    {
        public static class Assert
        {
            public static void Fail(string message)
                => throw new Xunit.Sdk.XunitException(message);
        }
    }
}
