﻿using FileService.Common.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace FileService.IntegrationTests.BlobTests
{
    public class FileStorageTestFixture : IAsyncLifetime
    {
        public string FilePath = string.Empty;
        public string FileName { get; } = $"{StringHelper.GetRandomString(6)}";
        public CloudBlobContainer BlobContainer { get; set; }
        public async Task InitializeAsync()
        {
            FilePath = Path.Combine(Path.GetTempPath(), $"{StringHelper.GetRandomString(6)}.txt");
            var result = await Task.Run(() => FileHelper.CreateFile(FilePath));
            if (!result)
            {
                TestHelper.Assert.Fail("Cannot create test file");
            }

            if (!await CreateTestStorage())
            {
                TestHelper.Assert.Fail("Could not create test storage");
            }
        }

        private async Task<bool> CreateTestStorage()
        {
            try
            {
                var client = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudBlobClient();
                var container = client.GetContainerReference("testcontainer");
                await container.CreateIfNotExistsAsync();

                BlobContainer = container;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> DeleteTestStorage()
        {
            try
            {
                await BlobContainer.DeleteIfExistsAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task DisposeAsync()
        {
            if (!await DeleteTestStorage())
            {
                TestHelper.Assert.Fail("Could not delete test storage");
            }

            if (!await Task.Run(() => FileHelper.DeleteFile(FilePath)))
            {
                TestHelper.Assert.Fail("Could not delete test file");
            }
        }
    }
}
