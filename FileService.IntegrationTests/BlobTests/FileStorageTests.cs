﻿using FileService.Persistence.Blobs;
using Microsoft.Extensions.Logging;
using Moq;
using System.IO;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions.Ordering;

namespace FileService.IntegrationTests.BlobTests
{
    /// <summary>
    /// Tests for blob testcontainer
    /// </summary>
    [Collection("Sequential")]
    public class FileStorageTests : IClassFixture<FileStorageTestFixture>
    {
        private readonly FileStorageTestFixture _testFixture;


        public FileStorageTests(FileStorageTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact, Order(1)]
        public async Task UploadFileToBlob_FileUploaded()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var result = await Record.ExceptionAsync(() => storage.UploadFileAsync(_testFixture.FilePath, _testFixture.FileName));
            Assert.Null(result);
        }

        [Fact, Order(2)]
        public async Task FileExistsOnBlobWhenFileExists_ReturnsTrue()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileExists = await storage.DoesFileExistsAsync(_testFixture.FileName);

            Assert.True(fileExists);
        }

        [Fact, Order(3)]
        public async Task GetFilesUrls_UrlsReturned()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileUrls = await storage.GetAllFileUrls();

            Assert.NotEmpty(fileUrls);
        }

        [Fact, Order(4)]
        public async Task GetFilesUrlsWithGivenExtension_UrlsReturned()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileUrls = await storage.GetAllFileUrls("application/octet-stream");

            Assert.NotEmpty(fileUrls);
        }

        [Fact, Order(5)]
        public async Task GetFileUrl_UrlReturned()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileUrl = await storage.GetFileUrlAsync(_testFixture.FileName);

            Assert.NotNull(fileUrl);
            Assert.Contains(_testFixture.FileName, fileUrl);
        }

        [Fact, Order(6)]
        public async Task DownloadFile_FileIsDownloaded()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileName = Path.GetFileNameWithoutExtension(_testFixture.FilePath);
            var exception = await Record.ExceptionAsync(() => storage.DownloadFileAsync(fileName, _testFixture.FileName));

            Assert.Null(exception);
        }

        [Fact, Order(7)]
        public async Task DeleteFileFromBlob_FileDeleted()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var exception = await Record.ExceptionAsync(() => storage.RemoveFileAsync(_testFixture.FileName));

            Assert.Null(exception);
        }

        [Fact, Order(8)]
        public async Task FileExistsOnBlobWhenFileNotExists_ReturnsFalse()
        {
            var containerProvider = new Mock<IAzureStorageContainerProvider>();
            containerProvider.Setup(x => x.ConfigureBlobContainer(It.IsAny<string>())).ReturnsAsync(_testFixture.BlobContainer);
            var logger = new Mock<ILogger<FileStorage>>();
            var storage = new FileStorage(containerProvider.Object, logger.Object);
            var fileExists = await storage.DoesFileExistsAsync(_testFixture.FileName);

            Assert.False(fileExists);
        }
    }
}
