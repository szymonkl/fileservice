﻿using FileService.Common.Helpers;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileService.IntegrationTests.RepositoryTests
{
    public class FileRepositoryTestFixture : IAsyncLifetime
    {
        public Mock<ICloudTableProvider> CloudTableProvider { get; private set; }
        public FileRepository FileRepository { get; private set; }
        public FileEntity FileEntity { get; private set; }
        public CloudTable TestTable { get; set; }
        private const string TestTableName = "filesMetadataTest";

        public async Task InitializeAsync()
        {
            var logger = Mock.Of<ILogger<BaseRepository<FileEntity>>>();
            CloudTableProvider = new Mock<ICloudTableProvider>();

            CloudTable testTable = await GetTestTableAsync(TestTableName);
            CloudTableProvider.Setup(x => x.TryConfigureStorageTable(TestTableName, out testTable))
                .ReturnsAsync(true);
            TestTable = testTable;
            FileRepository = new FileRepository(CloudTableProvider.Object, TestTableName, logger);

            FileEntity = new FileEntity
            {
                PartitionKey = StringHelper.GetRandomString(5),
                RowKey = StringHelper.GetRandomString(10),
                Name = StringHelper.GetRandomString(10),
                Type = "voice_announcement",
                Timestamp = DateTimeOffset.Now,
                UploadTime = DateTime.Now,
                CustomMetadata = new Dictionary<string, object> { ["Language"] = "English", ["isTTS"] = false }
            };
        }

        private static async Task<CloudTable> GetTestTableAsync(string tableName)
        {
            CloudTableClient tableClient = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient();
            var cloudTable = tableClient.GetTableReference(tableName);
            await cloudTable.CreateIfNotExistsAsync();
            return cloudTable;
        }

        public async Task DisposeAsync()
        {
            var result = await TestTable.DeleteIfExistsAsync();
            Assert.True(result, $"{TestTableName} was not deleted after tests execution");
        }
    }
}
