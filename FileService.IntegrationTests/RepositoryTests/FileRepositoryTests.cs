﻿using FileService.Exceptions;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions.Ordering;

namespace FileService.IntegrationTests.RepositoryTests
{
    /// <summary>
    /// CRUD operations tests for filesMetadataTest table
    /// </summary>
    [Collection("Sequential")]
    public class FileRepositoryTests : IClassFixture<FileRepositoryTestFixture>
    {
        private readonly FileRepositoryTestFixture _testFixture;
        private const string UpdatedEntityType = "language_pack";

        public FileRepositoryTests(FileRepositoryTestFixture testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact, Order(1)]
        public async Task AddEntity_EntityAdded()
        {
            CloudTable cloudTable = _testFixture.TestTable;

            var exception = await Record.ExceptionAsync(() => _testFixture.FileRepository.AddEntity(_testFixture.FileEntity));

            _testFixture.CloudTableProvider.Verify(x => x.TryConfigureStorageTable(It.IsAny<string>(), out cloudTable), Times.Once);
            Assert.Null(exception);
        }

        [Fact, Order(2)]
        public async Task GetEntity_EntityRetrieved()
        {
            var result = await _testFixture.FileRepository.Get(_testFixture.FileEntity.PartitionKey, _testFixture.FileEntity.RowKey);
            Assert.NotNull(result);
            Assert.Equal(_testFixture.FileEntity.Name, result.Name);
            Assert.Equal(_testFixture.FileEntity.Type, result.Type);
            Assert.Equal(_testFixture.FileEntity.UploadTime.ToFileTimeUtc(), result.UploadTime.ToFileTimeUtc());
            Assert.Equal(_testFixture.FileEntity.Timestamp.ToUniversalTime(), result.Timestamp.ToUniversalTime());
            Assert.Equal(_testFixture.FileEntity.CustomMetadata.Count, result.CustomMetadata.Count);
        }

        [Fact, Order(3)]
        public async Task UpdateEntity_EntityUpdated()
        {
            CloudTable cloudTable = _testFixture.TestTable;
            var updatedEntity = _testFixture.FileEntity;
            updatedEntity.Type = UpdatedEntityType;
            updatedEntity.CustomMetadata = new Dictionary<string, object> { ["Language"] = "French" };

            var exception = await Record.ExceptionAsync(() => _testFixture.FileRepository.Edit(updatedEntity));

            _testFixture.CloudTableProvider.Verify(x => x.TryConfigureStorageTable(It.IsAny<string>(), out cloudTable), Times.Once);
            Assert.Null(exception);
        }

        [Fact, Order(4)]
        public async Task GetAllEntities_EntitiesRetrieved()
        {
            var result = await _testFixture.FileRepository.GetAll(CancellationToken.None);

            Assert.NotNull(result);
            var fileEntities = result.ToList();

            Assert.Single(fileEntities);
            Assert.Equal(UpdatedEntityType, fileEntities.FirstOrDefault()?.Type);
            Assert.Equal(1, fileEntities.FirstOrDefault()?.CustomMetadata.Count);
        }

        [Fact, Order(5)]
        public async Task DeleteEntity_EntityDeleted()
        {
            var exception = await Record.ExceptionAsync(() => _testFixture.FileRepository.Delete(_testFixture.FileEntity));
            Assert.Null(exception);
            var result = await _testFixture.FileRepository.GetAll(CancellationToken.None);
            Assert.Empty(result);
        }

        [Fact, Order(6)]
        public async Task GetAllEntitiesOperationCancelled_ThrowsException()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            var result = await Record.ExceptionAsync(() => _testFixture.FileRepository.GetAll(cancellationToken));

            Assert.NotNull(result);
            Assert.True(result is FileRepositoryException);
        }
    }
}
