﻿using FileService.Application.Models;
using FileService.Controllers;
using MediatR;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OData;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.ControllerTests
{
    public class FilesControllerTests
    {
        [Fact]
        public async Task GetAsync_SendsGetFilesMetadataCommand()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            _ = await filesController.GetAsync(It.IsAny<ODataQueryOptions<FileMetadata>>());

            mediator.Verify(x => x.Send(It.IsAny<GetFilesMetadata>(), CancellationToken.None), Times.Once);
        }

        [Fact]
        public async Task GetAsync_SendsGetFileMetadataCommand()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            _ = await filesController.GetAsync("voice_announcement-zone_one", It.IsAny<ODataQueryOptions<FileMetadata>>());

            mediator.Verify(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None), Times.Once);
        }

        [Fact]
        public async Task GetByTypeByNameAsync_SendsGetFileMetadataCommand()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            _ = await filesController.ByTypeByName(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ODataQueryOptions<FileMetadata>>());

            mediator.Verify(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None), Times.Once);
        }

        [Fact]
        public async Task PostAsync_ReturnsCreated()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "https";
            httpContext.Request.Host = new HostString("localhost", 44393);
            httpContext.Request.Path = "/Files/";
            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object)
            { ControllerContext = controllerContext };

            var result = await filesController.PostAsync(new UploadFileRequest { Name = "zone_one" }) as CreatedResult;

            mediator.Verify(x => x.Send(It.IsAny<UploadFileRequest>(), CancellationToken.None), Times.Once);
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status201Created, result.StatusCode);
        }

        [Fact]
        public async Task PutAsync_ReturnsAccepted()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "https";
            httpContext.Request.Host = new HostString("localhost", 44393);
            httpContext.Request.Path = "/Files/";
            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
            var mediator = new Mock<IMediator>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var logger = new Mock<ILogger<FilesController>>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object)
            { ControllerContext = controllerContext };

            var result = await filesController.PutAsync(new UpdateFileMetadata { Name = "zone_one" }) as AcceptedResult;

            mediator.Verify(x => x.Send(It.IsAny<UpdateFileMetadata>(), CancellationToken.None), Times.Once);
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status202Accepted, result.StatusCode);
        }

        [Fact]
        public async Task DeleteAsync_ReturnsNotFound()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.DeleteAsync("voice_announcement-zone_one") as StatusCodeResult;

            mediator.Verify(x => x.Send(It.IsAny<DeleteFileRequest>(), CancellationToken.None), Times.Once);
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status404NotFound, result.StatusCode);
        }

        [Fact]
        public async Task GetAsync_SendsGetFilesMetadataCommandThrowsException_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            mediator.Setup(x => x.Send(It.IsAny<GetFilesMetadata>(), CancellationToken.None))
                .Throws<Exception>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.GetAsync(It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetAsync_SendsGetFilesMetadataCommandThrowsODataException_Returns400()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            mediator.Setup(x => x.Send(It.IsAny<GetFilesMetadata>(), CancellationToken.None))
                .Throws<ODataException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.GetAsync(It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetAsync_SendsGetFileMetadataCommandThrowsException_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);
            mediator.Setup(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None))
                .Throws<Exception>();

            var result = await filesController.GetAsync(It.IsAny<string>(), It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetAsync_SendsGetFileMetadataCommandThrowsODataException_Returns400()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);
            mediator.Setup(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None))
                .Throws<ODataException>();

            var result = await filesController.GetAsync("voice_announcement-zone_one", It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetByTypeByNameAsync_SendsGetFileMetadataCommandThrowsODataException_Returns400()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);
            mediator.Setup(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None))
                .Throws<ODataException>();

            var result = await filesController.ByTypeByName(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetByTypeByNameAsync_SendsGetFileMetadataCommandThrowsException_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);
            mediator.Setup(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None))
                .Throws<Exception>();

            var result = await filesController.ByTypeByName(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task PostAsync_CreateFileMetadataThrowsException_Returns500()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "https";
            httpContext.Request.Host = new HostString("localhost", 44393);
            httpContext.Request.Path = "/Files/";
            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            mediator.Setup(x => x.Send(It.IsAny<UploadFileRequest>(), CancellationToken.None))
                .Throws<Exception>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object)
            { ControllerContext = controllerContext };

            var result = await filesController.PostAsync(new UploadFileRequest { Name = "zone_one" }) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task PutAsync_UpdateFileMetadataThrowsException_Returns500()
        {
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "https";
            httpContext.Request.Host = new HostString("localhost", 44393);
            httpContext.Request.Path = "/Files/";
            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            mediator.Setup(x => x.Send(It.IsAny<UpdateFileMetadata>(), CancellationToken.None))
                .Throws<Exception>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object)
            { ControllerContext = controllerContext };

            var result = await filesController.PutAsync(new UpdateFileMetadata { Name = "zone_one" }) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task DeleteAsync_DeleteFileMetadataThrowsException_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            mediator.Setup(x => x.Send(It.IsAny<DeleteFileRequest>(), CancellationToken.None))
                .Throws<Exception>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.DeleteAsync(It.IsAny<string>()) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task GetAsync_GetFilesMetadataOperationCancelled_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();
            lifetime.Setup(x => x.ApplicationStopping).Returns(cancellationToken);
            mediator.Setup(x => x.Send(It.IsAny<GetFilesMetadata>(), cancellationToken))
                .Throws<OperationCanceledException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.GetAsync(It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetAsync_GetFileMetadataOperationCancelled_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();
            lifetime.Setup(x => x.ApplicationStopping).Returns(cancellationToken);
            mediator.Setup(x => x.Send(It.IsAny<GetFileMetadata>(), cancellationToken))
                .Throws<OperationCanceledException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.GetAsync(It.IsAny<string>(), It.IsAny<ODataQueryOptions<FileMetadata>>());

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, ((StatusCodeResult)result).StatusCode);
        }

        [Fact]
        public async Task GetAsync_CorruptedRequest_ReturnsBadRequest()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.GetAsync("corrupted", It.IsAny<ODataQueryOptions<FileMetadata>>());

            mediator.Verify(x => x.Send(It.IsAny<GetFileMetadata>(), CancellationToken.None), Times.Never);
            Assert.Equal(StatusCodes.Status400BadRequest, ((BadRequestObjectResult)result).StatusCode);
        }

        [Fact]
        public async Task PostAsync_CreateFileMetadataOperationCancelled_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();
            lifetime.Setup(x => x.ApplicationStopping).Returns(cancellationToken);
            mediator.Setup(x => x.Send(It.IsAny<UploadFileRequest>(), cancellationToken))
                .Throws<OperationCanceledException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.PostAsync(It.IsAny<UploadFileRequest>()) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task PutAsync_UpdateFileMetadataOperationCancelled_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();
            lifetime.Setup(x => x.ApplicationStopping).Returns(cancellationToken);
            mediator.Setup(x => x.Send(It.IsAny<UpdateFileMetadata>(), cancellationToken))
                .Throws<OperationCanceledException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.PutAsync(It.IsAny<UpdateFileMetadata>()) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task DeleteAsync_DeleteFileMetadataOperationCancelled_Returns500()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();
            lifetime.Setup(x => x.ApplicationStopping).Returns(cancellationToken);
            mediator.Setup(x => x.Send(It.IsAny<DeleteFileRequest>(), cancellationToken))
                .Throws<OperationCanceledException>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.DeleteAsync(It.IsAny<string>()) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status500InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task DeleteAsync_DeleteFileMetadataInvalidRequest_Returns400()
        {
            var mediator = new Mock<IMediator>();
            var logger = new Mock<ILogger<FilesController>>();
            var lifetime = new Mock<IHostApplicationLifetime>();

            var filesController = new FilesController(mediator.Object, logger.Object, lifetime.Object);

            var result = await filesController.DeleteAsync("invalid") as BadRequestObjectResult;

            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status400BadRequest, result.StatusCode);
        }
    }
}
