﻿using FileService.Application.Commands;
using FileService.Application.Models;
using FileService.Exceptions;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.HandlerTests
{
    public class DeleteFileHandlerTests
    {
        [Fact]
        public async Task Handle_DeleteFile_IfPresentDeletedFromRepository()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            var entity = new FileEntity { PartitionKey = "amp", RowKey = "zone_one", Name = "zone_one" };
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(entity));
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);
            var request = new DeleteFileRequest("amp", "zone_one");

            _ = await handler.Handle(request, CancellationToken.None);

            repository.Verify(r => r.Delete(It.IsAny<FileEntity>()), Times.Once);
            storage.Verify(s => s.RemoveFileAsync(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Handle_DeleteFile_IfNotPresentNothingHappened()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult<FileEntity>(null));
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);
            var request = new DeleteFileRequest("amp", "zone_one");

            _ = await handler.Handle(request, CancellationToken.None);

            repository.Verify(r => r.Delete(It.IsAny<FileEntity>()), Times.Never);
            storage.Verify(r => r.RemoveFileAsync(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task Handle_DeleteFileOperationCancelled_ThrowsException()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            await Assert.ThrowsAnyAsync<OperationCanceledException>(
                () => handler.Handle(It.IsAny<DeleteFileRequest>(), cancellationToken));
            repository.Verify(x => x.Delete(It.IsAny<FileEntity>()), Times.Never);
            storage.Verify(x => x.RemoveFileAsync(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task Handle_DeleteFileStorageThrowsException_MetadataNotDeleted()
        {
            var request = new DeleteFileRequest("amp", "zone_one");
            var entity = new FileEntity();
            var repository = new Mock<IFileRepository>();
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(entity);
            var storage = new Mock<IFileStorage>();
            storage.Setup(x => x.RemoveFileAsync(It.IsAny<string>()))
                .Throws<FileStorageException>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);

            await Assert.ThrowsAnyAsync<FileStorageException>(
                () => handler.Handle(request, CancellationToken.None));
            repository.Verify(x => x.Delete(It.IsAny<FileEntity>()), Times.Never);
        }

        [Fact]
        public async Task Handle_DeleteFileMetadataThrowsException_ExceptionThrown()
        {
            var request = new DeleteFileRequest("amp", "zone_one");
            var entity = new FileEntity();
            var repository = new Mock<IFileRepository>();
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(entity);
            repository.Setup(x => x.Delete(It.IsAny<FileEntity>())).Throws<FileRepositoryException>();
            var storage = new Mock<IFileStorage>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);

            await Assert.ThrowsAnyAsync<FileRepositoryException>(
                () => handler.Handle(request, CancellationToken.None));
        }

        [Fact]
        public async Task Handle_UnknownException_ExceptionThrown()
        {
            var request = new DeleteFileRequest("amp", "zone_one");
            var repository = new Mock<IFileRepository>();
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();
            var storage = new Mock<IFileStorage>();
            var logger = new Mock<ILogger<DeleteFileHandler>>();
            var handler = new DeleteFileHandler(repository.Object, storage.Object, logger.Object);

            await Assert.ThrowsAnyAsync<Exception>(
                () => handler.Handle(request, CancellationToken.None));
        }
    }
}
