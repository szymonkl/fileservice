﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Application.Queries;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.HandlerTests
{
    public class GetFileMetadataHandlerTests
    {
        [Fact]
        public async Task Handle_GetFileMetadata_GetOneFromRepository()
        {
            var repository = new Mock<IFileRepository>();
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new FileEntity());
            var storage = new Mock<IFileStorage>();
            storage.Setup(x => x.DoesFileExistsAsync(It.IsAny<string>())).ReturnsAsync(true);
            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<FileMetadata>(It.IsAny<FileEntity>())).Returns(new FileMetadata());
            var logger = new Mock<ILogger<GetFileMetadataHandler>>();
            var handler = new GetFileMetadataHandler(repository.Object, storage.Object, mapper.Object, logger.Object);
            var command = new GetFileMetadata("amp", "zone_one");

            var result = await handler.Handle(command, CancellationToken.None);

            repository.Verify(r => r.Get(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            storage.Verify(s => s.DoesFileExistsAsync(It.IsAny<string>()), Times.Once);
            Assert.True(result.ExistsOnBlob);
        }

        [Fact]
        public async Task Handle_GetFileMetadataOperationCancelled_ThrowsException()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<GetFileMetadataHandler>>();
            var handler = new GetFileMetadataHandler(repository.Object, storage.Object, mapper.Object, logger.Object);
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            await Assert.ThrowsAnyAsync<OperationCanceledException>(
                () => handler.Handle(It.IsAny<GetFileMetadata>(), cancellationToken));
            repository.Verify(x => x.Get(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }
    }
}
