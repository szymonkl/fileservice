﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Application.Queries;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.HandlerTests
{
    public class GetFilesMetadataHandlerTests
    {
        [Fact]
        public async Task Handle_GetFilesMetadata_GetAllFromRepository()
        {
            var repository = new Mock<IFileRepository>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<GetFilesMetadataHandler>>();
            var handler = new GetFilesMetadataHandler(repository.Object, mapper.Object, logger.Object);

            _ = await handler.Handle(It.IsAny<GetFilesMetadata>(), CancellationToken.None);

            repository.Verify(r => r.GetAll(CancellationToken.None), Times.Once);
        }

        [Fact]
        public async Task Handle_GetFilesMetadataOperationCancelled_ThrowsException()
        {
            var repository = new Mock<IFileRepository>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<GetFilesMetadataHandler>>();
            var handler = new GetFilesMetadataHandler(repository.Object, mapper.Object, logger.Object);
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            await Assert.ThrowsAnyAsync<OperationCanceledException>(
                () => handler.Handle(It.IsAny<GetFilesMetadata>(), cancellationToken));
            repository.Verify(x => x.GetAll(CancellationToken.None), Times.Never);
        }
    }
}
