﻿using AutoMapper;
using FileService.Application.Commands;
using FileService.Application.Models;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.HandlerTests
{
    public class UpdateFileMetadataHandlerTests
    {
        [Fact]
        public async Task Handle_UpdateFileMetadata_UpdateItInRepository()
        {
            var repository = new Mock<IFileRepository>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<UpdateFileMetadataHandler>>();
            var handler = new UpdateFileMetadataHandler(repository.Object, mapper.Object, logger.Object);
            var request = new FileEntity
            {
                PartitionKey = "amp",
                RowKey = "zone_one",
                Timestamp = DateTimeOffset.Now,
                UploadTime = DateTime.Now,
                Name = "zone_one",
                CustomMetadata = new Dictionary<string, object>(),
                Type = "voice_announcement"
            };

            mapper.Setup(x => x.Map<FileEntity>(It.IsAny<UpdateFileMetadata>())).Returns(request);
            _ = await handler.Handle(It.IsAny<UpdateFileMetadata>(), CancellationToken.None);

            repository.Verify(r => r.Edit(It.IsAny<FileEntity>()), Times.Once);
        }

        [Fact]
        public async Task Handle_UpdateFileMetadataOperationCancelled_ThrowsException()
        {
            var repository = new Mock<IFileRepository>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<UpdateFileMetadataHandler>>();
            var handler = new UpdateFileMetadataHandler(repository.Object, mapper.Object, logger.Object);
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            await Assert.ThrowsAnyAsync<OperationCanceledException>(
                () => handler.Handle(new UpdateFileMetadata(), cancellationToken));
            repository.Verify(x => x.Edit(It.IsAny<FileEntity>()), Times.Never);
        }
    }
}
