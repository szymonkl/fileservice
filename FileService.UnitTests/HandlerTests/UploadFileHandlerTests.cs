using AutoMapper;
using FileService.Application.Commands;
using FileService.Application.Models;
using FileService.Exceptions;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FileService.UnitTests.HandlerTests
{
    public class UploadFileHandlerTests
    {
        [Fact]
        public async Task UploadFile_FileUploadedMetadataInserted()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            storage.Setup(x => x.DoesFileExistsAsync(It.IsAny<string>())).ReturnsAsync(true);
            var mapper = new Mock<IMapper>();
            var logger = Mock.Of<ILogger<UploadFileHandler>>();
            var handler = new UploadFileHandler(repository.Object, storage.Object, mapper.Object, logger);

            mapper.Setup(x => x.Map<FileEntity>(It.IsAny<UploadFileRequest>())).Returns(new FileEntity());
            mapper.Setup(x => x.Map<FileMetadata>(It.IsAny<FileEntity>())).Returns(new FileMetadata());
            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new FileEntity());
            _ = await handler.Handle(new UploadFileRequest(), CancellationToken.None);

            repository.Verify(r => r.AddEntity(It.IsAny<FileEntity>()), Times.Once);
            storage.Verify(s => s.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task UploadFile_OperationCancelled_FileNotUploadedMetadataNotInserted()
        {
            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var mapper = new Mock<IMapper>();
            var logger = new Mock<ILogger<UploadFileHandler>>();
            var handler =
                new UploadFileHandler(repository.Object, storage.Object, mapper.Object, logger.Object);
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            cancellationTokenSource.Cancel();

            await Assert.ThrowsAnyAsync<OperationCanceledException>(
                () => handler.Handle(It.IsAny<UploadFileRequest>(), cancellationToken));
            repository.Verify(x => x.AddEntity(It.IsAny<FileEntity>()), Times.Never);
            storage.Verify(x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task UploadFile_RepositoryThrowsException_FileNotUploaded()
        {

            var entity = new FileEntity
            {
                UploadingStatus = (int)UploadingStatus.InProgress
            };

            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<FileEntity>(It.IsAny<UploadFileRequest>())).Returns(entity);
            var logger = Mock.Of<ILogger<UploadFileHandler>>();
            var handler = new UploadFileHandler(repository.Object, storage.Object, mapper.Object, logger);

            repository.Setup(x => x.AddEntity(It.IsAny<FileEntity>())).Throws<FileRepositoryException>();
            var exception = await Record.ExceptionAsync(() => handler.Handle(new UploadFileRequest(), CancellationToken.None));

            Assert.IsType<FileRepositoryException>(exception);
            storage.Verify(s => s.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task UploadFile_StorageThrowsException_MetadataRemoved()
        {
            var request = new UploadFileRequest
            {
                Target = "amp",
                Name = "zone_one",
                LocalPath = @"C:\Users\User1\Downloads\object.jpg",
                Type = "picture"
            };
            var entity = new FileEntity
            {
                UploadingStatus = (int)UploadingStatus.InProgress
            };

            var repository = new Mock<IFileRepository>();
            var storage = new Mock<IFileStorage>();
            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<FileEntity>(It.IsAny<UploadFileRequest>())).Returns(entity);
            var logger = Mock.Of<ILogger<UploadFileHandler>>();
            var handler = new UploadFileHandler(repository.Object, storage.Object, mapper.Object, logger);
            storage.Setup(x => x.UploadFileAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<FileStorageException>();

            var exception = await Record.ExceptionAsync(() => handler.Handle(request, CancellationToken.None));

            Assert.IsType<FileStorageException>(exception);
            repository.Verify(r => r.Delete(It.IsAny<FileEntity>()), Times.Once);
        }

        [Fact]
        public async Task UploadFile_UnknownExceptionThrown_ExceptionHandled()
        {
            var repository = new Mock<IFileRepository>();
            var request = new UploadFileRequest
            {
                Target = "amp",
                Name = "zone_one",
                LocalPath = @"C:\Users\User1\Downloads\object.jpg",
                Type = "picture"
            };
            var storage = new Mock<IFileStorage>();
            storage.Setup(x => x.DoesFileExistsAsync(It.IsAny<string>())).ReturnsAsync(true);

            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<FileMetadata>(It.IsAny<FileEntity>())).Returns(new FileMetadata());
            var logger = new Mock<ILogger<UploadFileHandler>>();
            var handler = new UploadFileHandler(repository.Object, storage.Object, mapper.Object, logger.Object);

            mapper.Setup(x => x.Map<FileEntity>(It.IsAny<UploadFileRequest>())).Returns(new FileEntity());

            repository.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>()))
                .Throws<Exception>();

            var exception = await Record.ExceptionAsync(() => handler.Handle(request, CancellationToken.None));

            Assert.IsType<Exception>(exception);
        }
    }
}
