﻿using FileService.Infrastructure;
using FileService.Persistence.Tables;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Xunit;

namespace FileService.UnitTests.ProfileTests
{
    public class EntityJsonPropertyConverterTests
    {
        private const string ComplexObject = "ComplexObject";
        private const string CustomMetadata = "CustomMetadata";
        private const string English = "English";
        private const string Language = "Language";
        private const string TestAgency = "test_agency";
        private const string ZoneOne = "zone_one";

        [Fact]
        public void ConvertEntityWithJsonProperty_ConversionSucceeds()
        {
            var entity = new FileEntity { CustomMetadata = new Dictionary<string, object> { [Language] = English } };
            var properties = new Dictionary<string, EntityProperty>();

            EntityJsonPropertyConverter.Serialize(entity, properties);

            Assert.Single(properties);
            Assert.NotNull(properties[CustomMetadata]);

            var deserializedEntity = new FileEntity();

            EntityJsonPropertyConverter.Deserialize(deserializedEntity, properties);

            Assert.NotNull(deserializedEntity);
            Assert.Single(deserializedEntity.CustomMetadata);
        }

        [Fact]
        public void ConvertEntityWithNullJsonProperty_ConversionSucceeds()
        {
            var entity = new FileEntity { Name = ZoneOne, CustomMetadata = null };
            var properties = new Dictionary<string, EntityProperty>();

            EntityJsonPropertyConverter.Serialize(entity, properties);

            Assert.NotNull(properties);
            Assert.NotNull(properties[CustomMetadata]);

            var deserializedEntity = new FileEntity();

            EntityJsonPropertyConverter.Deserialize(deserializedEntity, properties);

            Assert.NotNull(deserializedEntity);
            Assert.Null(deserializedEntity.CustomMetadata);
        }

        [Fact]
        public void ConvertEntityWithComplexJsonProperty_ConversionSucceeds()
        {
            var entity = new FileEntity
            {
                Name = ZoneOne,
                CustomMetadata = new Dictionary<string, object>
                {
                    [ComplexObject] = new { Id = 1, Agency = TestAgency }
                }
            };
            var properties = new Dictionary<string, EntityProperty>();

            EntityJsonPropertyConverter.Serialize(entity, properties);

            Assert.NotNull(properties);
            Assert.NotNull(properties[CustomMetadata]);

            var deserializedEntity = new FileEntity();

            EntityJsonPropertyConverter.Deserialize(deserializedEntity, properties);

            Assert.NotNull(deserializedEntity);
            Assert.Single(deserializedEntity.CustomMetadata);

            var complexObject = JObject.Parse(deserializedEntity.CustomMetadata[ComplexObject].ToString());
            var id = complexObject["Id"];
            Assert.Equal(1, (int)id);
            var agency = complexObject["Agency"];
            Assert.Equal(TestAgency, agency);
        }
    }
}
