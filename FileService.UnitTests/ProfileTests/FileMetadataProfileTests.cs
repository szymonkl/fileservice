﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Infrastructure;
using FileService.Persistence.Tables;
using System;
using System.Collections.Generic;
using Xunit;

namespace FileService.UnitTests.ProfileTests
{
    public class FileMetadataProfileTests
    {
        private static readonly FileEntity Entity = new FileEntity
        {
            PartitionKey = "voice_announcement",
            RowKey = "zone_one",
            Name = "zone_one",
            Type = "voice_announcement",
            Url = "http://127.0.0.1:10000/devstoreaccount1/filestorage/zone_one?sv=2018-03-28&sr=b&sig=JCV1491NcG8DI64jHkp6h8lCqnIsbOsgImw3LHmB68M%3D&st=2021-01-05T21%3A13%3A29Z&se=2021-01-05T21%3A23%3A29Z&sp=rl",
            UploadTime = DateTime.Now,
            Timestamp = DateTimeOffset.Now,
            CustomMetadata = new Dictionary<string, object> { ["Language"] = "English", ["isTTS"] = false }
        };

        [Fact]
        public void FileMetadataProfile_ConfigurationValid()
        {
            var exception = Record.Exception(CreateMapper);
            Assert.Null(exception);
        }

        [Fact]
        public void FileMetadataProfile_Map_Succeeds()
        {
            var mapper = CreateMapper();
            var fileMetadata = mapper.Map<FileMetadata>(Entity);
            Assert.NotNull(fileMetadata);
            Assert.Equal(Entity.Name, fileMetadata.Name);
            Assert.Equal(Entity.RowKey, fileMetadata.Name);
            Assert.Contains(Entity.Name, fileMetadata.Url);
            Assert.Equal(Entity.PartitionKey, fileMetadata.Type);
            Assert.Equal(Entity.Type, fileMetadata.Type);
            Assert.Equal(Entity.CustomMetadata["Language"], fileMetadata.CustomMetadata["Language"]);
            Assert.Equal(Entity.CustomMetadata["isTTS"], fileMetadata.CustomMetadata["isTTS"]);
        }

        [Fact]
        public void FileMetadataProfile_InvalidMap_Fails()
        {
            var mapper = CreateMapper();
            Assert.Throws<AutoMapperMappingException>(
                () => mapper.Map<FileMetadata>(new object()));
        }

        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<FileMetadataProfile>();
            });
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
    }
}
