﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Infrastructure;
using FileService.Persistence.Tables;
using System.Collections.Generic;
using Xunit;

namespace FileService.UnitTests.ProfileTests
{
    public class UpdateFileMetadataProfileTests
    {
        private static readonly UpdateFileMetadata Command = new UpdateFileMetadata
        {
            Target = "amp",
            Name = "zone_one",
            Type = "voice_announcement",
            CustomMetadata = new Dictionary<string, object> { ["Language"] = "English", ["isTTS"] = false }
        };

        [Fact]
        public void UpdateFileMetadataProfile_ConfigurationValid()
        {
            var exception = Record.Exception(CreateMapper);
            Assert.Null(exception);
        }

        [Fact]
        public void UpdateFileMetadataProfile_Map_Succeeds()
        {
            var mapper = CreateMapper();
            var entity = mapper.Map<FileEntity>(Command);
            Assert.NotNull(entity);
            Assert.Equal(Command.Name, entity.Name);
            Assert.Equal(Command.Name, entity.RowKey);
            Assert.Equal(Command.Type, entity.PartitionKey);
            Assert.Equal(Command.Type, entity.Type);
            Assert.Equal(Command.Target, entity.Target);
            Assert.Equal(Command.CustomMetadata, entity.CustomMetadata);
        }

        [Fact]
        public void UpdateFileMetadataProfile_InvalidMap_Fails()
        {
            var mapper = CreateMapper();
            Assert.Throws<AutoMapperMappingException>(
                () => mapper.Map<FileEntity>(new object()));
        }

        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UpdateFileMetadataProfile>();
            });
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
    }
}
