﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Infrastructure;
using FileService.Persistence.Tables;
using System.Collections.Generic;
using Xunit;

namespace FileService.UnitTests.ProfileTests
{
    public class UploadFileProfileTests
    {
        private static readonly UploadFileRequest Command = new UploadFileRequest
        {
            Target = "amp",
            Name = "zone_one",
            Type = "voice_announcement",
            LocalPath = @"C:\Users\User1\Downloads\object.jpg",
            CustomMetadata = new Dictionary<string, object>() { ["Language"] = "English", ["isTTS"] = true }
        };

        [Fact]
        public void UploadFileProfile_ConfigurationValid()
        {
            var exception = Record.Exception(CreateMapper);
            Assert.Null(exception);
        }

        [Fact]
        public void UploadFileProfile_Map_Succeeds()
        {
            var mapper = CreateMapper();
            var entity = mapper.Map<FileEntity>(Command);
            Assert.NotNull(entity);
            Assert.Equal(Command.Name, entity.Name);
            Assert.Equal(Command.Name, entity.RowKey);
            Assert.Contains(entity.Extension, Command.LocalPath);
            Assert.Equal(Command.Type, entity.PartitionKey);
            Assert.Equal(Command.Type, entity.Type);
            Assert.Equal(Command.CustomMetadata["Language"], entity.CustomMetadata["Language"]);
            Assert.Equal(Command.CustomMetadata["isTTS"], entity.CustomMetadata["isTTS"]);
        }

        [Fact]
        public void UploadFileProfile_InvalidMap_Fails()
        {
            var mapper = CreateMapper();
            Assert.Throws<AutoMapperMappingException>(
                () => mapper.Map<FileEntity>(new object()));
        }

        private static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<UploadFileProfile>();
                });
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
    }
}
