﻿using FileService.Application.Validations;
using Xunit;

namespace FileService.UnitTests.ValidationsTests
{
    public class KeyValidatorTests
    {
        [Fact]
        public void Validate_ValidKey_ReturnsTrue()
        {
            var result = KeyValidator.Validate("voice_announcement-zone_one_english");
            Assert.True(result);
        }

        [Fact]
        public void Validate_MultipleSeparators_ReturnsFalse()
        {
            var result = KeyValidator.Validate("voice-announcement-zone_one_english");
            Assert.False(result);
        }

        [Fact]
        public void Validate_NoSeparators_ReturnsFalse()
        {
            var result = KeyValidator.Validate("zone_one");
            Assert.False(result);
        }

        [Fact]
        public void Validate_EmptyString_ReturnsFalse()
        {
            var result = KeyValidator.Validate(string.Empty);
            Assert.False(result);
        }

        [Fact]
        public void Validate_OnlySeparator_ReturnsFalse()
        {
            var result = KeyValidator.Validate("-");
            Assert.False(result);
        }

        [Fact]
        public void Validate_OnlyTwoSeparators_ReturnsFalse()
        {
            var result = KeyValidator.Validate("--");
            Assert.False(result);
        }

        [Fact]
        public void Split_ReturnsSplitWords()
        {
            var (type, name) = KeyValidator.Split("voice_announcement-zone_one_english");
            Assert.Equal("voice_announcement", type);
            Assert.Equal("zone_one_english", name);
        }
    }

}
