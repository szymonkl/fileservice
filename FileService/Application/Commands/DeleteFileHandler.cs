﻿using FileService.Application.Models;
using FileService.Exceptions;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Application.Commands
{
    public class DeleteFileHandler : IRequestHandler<DeleteFileRequest, bool>
    {
        private readonly IFileRepository _repository;
        private readonly IFileStorage _storage;
        private readonly ILogger<DeleteFileHandler> _logger;

        public DeleteFileHandler(IFileRepository repository, IFileStorage storage, ILogger<DeleteFileHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _storage = storage;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<bool> Handle(DeleteFileRequest request, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Request_terminating, nameof(DeleteFileRequest)));
                throw new OperationCanceledException(nameof(DeleteFileRequest));
            }

            try
            {
                var entity = await _repository.Get(request.Target, request.Name);
                if (entity != null)
                {
                    await _storage.RemoveFileAsync(entity.Name);
                    await _repository.Delete(entity);
                    return await Task.FromResult(true);
                }

                return await Task.FromResult(false);
            }
            catch (FileStorageException ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Error_deleting_file, request.Name));
                throw;
            }
            catch (FileRepositoryException ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Error_deleting_metadata, request.Name));
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, Resources.Resources.Unknown_error);
                throw;
            }
        }
    }
}
