﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Application.Commands
{
    public class UpdateFileMetadataHandler : IRequestHandler<UpdateFileMetadata, FileMetadata>
    {
        private readonly IFileRepository _repository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateFileMetadataHandler> _logger;

        public UpdateFileMetadataHandler(IFileRepository repository, IMapper mapper, ILogger<UpdateFileMetadataHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<FileMetadata> Handle(UpdateFileMetadata request, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Request_terminating, nameof(UpdateFileMetadata)));
                throw new OperationCanceledException(nameof(UpdateFileMetadata));
            }

            var entity = _mapper.Map<FileEntity>(request);
            await _repository.Edit(entity);

            var updatedEntity = await _repository.Get(entity.PartitionKey, entity.RowKey);
            return _mapper.Map<FileMetadata>(updatedEntity);
        }
    }
}
