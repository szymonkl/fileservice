﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Exceptions;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Application.Commands
{
    public class UploadFileHandler : IRequestHandler<UploadFileRequest, FileMetadata>
    {
        private readonly IFileRepository _repository;
        private readonly IFileStorage _storage;
        private readonly IMapper _mapper;
        private readonly ILogger<UploadFileHandler> _logger;

        public UploadFileHandler(IFileRepository repository, IFileStorage storage, IMapper mapper, ILogger<UploadFileHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<FileMetadata> Handle(UploadFileRequest request, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Request_terminating, nameof(UploadFileRequest)));
                throw new OperationCanceledException(nameof(UploadFileRequest));
            }

            try
            {
                var entity = _mapper.Map<FileEntity>(request);
                entity.UploadingStatus = (int)UploadingStatus.InProgress;
                await _repository.AddEntity(entity);
                await _storage.UploadFileAsync(request.LocalPath, request.Name);

                if (await _storage.DoesFileExistsAsync(request.Name))
                {
                    var createdEntity = await _repository.Get(entity.PartitionKey, entity.RowKey);
                    createdEntity.Url = await _storage.GetFileUrlAsync(request.Name);
                    createdEntity.UploadingStatus = (int)UploadingStatus.Completed;
                    await _repository.Edit(createdEntity);

                    var newMetadata = _mapper.Map<FileMetadata>(createdEntity);
                    newMetadata.ExistsOnBlob = true;

                    return newMetadata;
                }

                var failedMetadata = _mapper.Map<FileMetadata>(entity);
                failedMetadata.ExistsOnBlob = false;
                failedMetadata.Url = null;
                failedMetadata.UploadingStatus = UploadingStatus.Failed;
                return failedMetadata;
            }
            catch (FileRepositoryException ex)
            {
                _logger.LogError(ex, Resources.Resources.Error_inserting_metadata);
                throw;
            }
            catch (FileStorageException ex)
            {
                _logger.LogError(ex, Resources.Resources.Error_uploading_file);
                var entity = _mapper.Map<FileEntity>(request);
                await _repository.Delete(entity);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, Resources.Resources.Unknown_error);
                throw;
            }
        }
    }
}
