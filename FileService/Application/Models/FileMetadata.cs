﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FileService.Application.Models
{
    public class FileMetadata
    {
        [Required]
        public string Target { get; set; }
        [Required]
        public string Name { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string Extension { get; set; }
        public DateTime UploadTime { get; set; }
        public bool ExistsOnBlob { get; set; }
        public UploadingStatus UploadingStatus { get; set; }
        public IDictionary<string, object> CustomMetadata { get; set; }
    }
}
