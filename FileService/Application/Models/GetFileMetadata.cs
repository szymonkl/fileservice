﻿using FileService.Application.Validations;
using FileService.Exceptions;
using MediatR;

namespace FileService.Application.Models
{
    public class GetFileMetadata : IRequest<FileMetadata>
    {
        public GetFileMetadata(string key)
        {
            if (KeyValidator.Validate(key))
            {
                var (type, name) = KeyValidator.Split(key);
                Target = type;
                Name = name;
            }
            else
            {
                throw new InvalidRequestException(Resources.Resources.Key_not_match);
            }
        }
        public GetFileMetadata(string target, string name)
        {
            Target = target;
            Name = name;
        }

        public string Target { get; set; }
        public string Name { get; set; }
    }
}
