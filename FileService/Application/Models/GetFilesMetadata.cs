﻿using MediatR;
using System.Linq;

namespace FileService.Application.Models
{
    public class GetFilesMetadata : IRequest<IQueryable<FileMetadata>>
    {
    }
}
