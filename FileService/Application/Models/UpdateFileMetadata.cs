﻿using MediatR;
using System.Collections.Generic;

namespace FileService.Application.Models
{
    public class UpdateFileMetadata : IRequest<FileMetadata>
    {
        public string Target { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Extension { get; set; }
        public UploadingStatus UploadingStatus { get; set; }
        public Dictionary<string, object> CustomMetadata { get; set; }
    }
}
