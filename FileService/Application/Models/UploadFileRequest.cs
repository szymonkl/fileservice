﻿using MediatR;
using System.Collections.Generic;

namespace FileService.Application.Models
{
    public class UploadFileRequest : IRequest<FileMetadata>
    {
        public string Target { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string LocalPath { get; set; }
        public IDictionary<string, object> CustomMetadata { get; set; }
    }
}
