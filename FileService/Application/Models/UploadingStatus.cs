﻿namespace FileService.Application.Models
{
    public enum UploadingStatus
    {
        InProgress = 0,
        Completed = 1,
        Failed = 2
    }
}
