﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Application.Queries
{
    public class GetFileMetadataHandler : IRequestHandler<GetFileMetadata, FileMetadata>
    {
        private readonly IFileRepository _repository;
        private readonly IFileStorage _storage;
        private readonly IMapper _mapper;
        private readonly ILogger<GetFileMetadataHandler> _logger;

        public GetFileMetadataHandler(IFileRepository repository, IFileStorage storage, IMapper mapper, ILogger<GetFileMetadataHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _storage = storage;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<FileMetadata> Handle(GetFileMetadata request, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Request_terminating, nameof(GetFileMetadata)));
                throw new OperationCanceledException(nameof(GetFileMetadata));
            }

            var fileEntity = await _repository.Get(request.Target, request.Name);
            if (fileEntity == null) return null;

            var metadata = _mapper.Map<FileMetadata>(fileEntity);
            metadata.ExistsOnBlob = await _storage.DoesFileExistsAsync(metadata.Name);

            return metadata;

        }
    }
}
