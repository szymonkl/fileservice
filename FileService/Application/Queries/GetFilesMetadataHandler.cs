﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Tables;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Application.Queries
{
    public class GetFilesMetadataHandler : IRequestHandler<GetFilesMetadata, IQueryable<FileMetadata>>
    {
        private readonly IFileRepository _repository;
        private readonly IMapper _mapper;
        private readonly ILogger<GetFilesMetadataHandler> _logger;

        public GetFilesMetadataHandler(IFileRepository repository, IMapper mapper, ILogger<GetFilesMetadataHandler> logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IQueryable<FileMetadata>> Handle(GetFilesMetadata request, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Request_terminating, nameof(GetFilesMetadata)));
                throw new OperationCanceledException(nameof(GetFilesMetadata));
            }

            var fileEntities = await _repository.GetAll(cancellationToken);
            var filesMetadata = _mapper.Map<IEnumerable<FileMetadata>>(fileEntities);
            return filesMetadata.AsQueryable();
        }
    }
}
