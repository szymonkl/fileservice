﻿using System.Text.RegularExpressions;

namespace FileService.Application.Validations
{
    public static class KeyValidator
    {
        private const string Separator = "-";
        public static bool Validate(string key) => Regex.IsMatch(key, $"^([^{Separator}]+{Separator}){{1}}[^{Separator}]+$");

        public static (string type, string name) Split(string key)
        {
            var arr = Regex.Split(key, Separator);
            return (arr[0], arr[1]);
        }
    }
}
