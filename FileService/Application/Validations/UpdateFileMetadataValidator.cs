﻿using FileService.Application.Models;
using FluentValidation;
using System;

namespace FileService.Application.Validations
{
    public class UpdateFileMetadataValidator : AbstractValidator<UpdateFileMetadata>
    {
        public UpdateFileMetadataValidator()
        {
            RuleFor(x => x.Name)
                .Length(1, 30);
            RuleFor(x => x.Target)
                .Length(1, 30);
            RuleFor(x => x.Type)
                .Length(1, 30);
            RuleFor(x => x.Extension)
                .Must(x => x.StartsWith(".")).WithMessage(Resources.Resources.Extension_starts_from)
                .Length(1, 10);
            RuleFor(x => x.UploadingStatus)
                .IsInEnum().WithMessage(string.Format(Resources.Resources.Allowed_uploading_statuses,
                    string.Join(", ", UploadingStatuses)));
        }

        private static string[] UploadingStatuses => Enum.GetNames(typeof(UploadingStatus));
    }
}
