﻿using FileService.Application.Models;
using FluentValidation;

namespace FileService.Application.Validations
{
    public class UploadFileRequestValidator : AbstractValidator<UploadFileRequest>
    {
        public UploadFileRequestValidator()
        {
            RuleFor(x => x.Name).Length(1, 30);
            RuleFor(x => x.Target).Length(1, 30);
            RuleFor(x => x.Type).Length(1, 30);
            RuleFor(x => x.LocalPath).Length(1, 150);
        }
    }
}
