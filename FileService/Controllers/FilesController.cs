﻿using FileService.Application.Models;
using FileService.Exceptions;
using FileService.Extensions;
using MediatR;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FileService.Controllers
{

    [ApiVersion("1.0")]
    public class FilesController : ODataController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<FilesController> _logger;
        private readonly IHostApplicationLifetime _applicationLifetime;

        public FilesController(IMediator mediator, ILogger<FilesController> logger, IHostApplicationLifetime applicationLifetime)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger;
            _applicationLifetime = applicationLifetime;
        }

        /// <summary>
        /// Allows querying files metadata by defined and dynamic properties using OData standard
        /// </summary>
        /// <response code="200">`OK` <br/>The operation was successful. The response body includes requested file metadata</response>
        /// <response code="400">`Bad request` <br/>The request was invalid or not processable using OData standard</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="options">The current OData query options.</param>
        /// <returns>Metadata entity with requested properties</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<FileMetadata>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAsync(ODataQueryOptions<FileMetadata> options)
        {
            try
            {
                var filesMetadata =
                    await _mediator.Send(new GetFilesMetadata(), _applicationLifetime.ApplicationStopping);
                return Ok(options.ApplyTo(filesMetadata));
            }
            catch (ODataException ex)
            {
                _logger.LogError(ex, Resources.Resources.OData_error);
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, Resources.Resources.General_error_get_request);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Allows querying files metadata by file name using OData standard
        /// </summary>
        /// <response code="200">`OK` <br/>The operation was successful. The response body includes requested file metadata</response>
        /// <response code="400">`Bad request` <br/>The request was invalid or not processable using OData standard</response>
        /// <response code="404">`Not found` <br/>There is no file metadata which meets given criteria.</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="key">Key is a combination of type(partition key) and name (row key) separated by dash i.e. voice_announcement-zone_one_english</param>
        /// <param name="options">The current OData query options.</param>
        /// <returns>Metadata entity with requested properties</returns>
        [Produces("application/json")]
        [ProducesResponseType(typeof(FileMetadata), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAsync(string key, ODataQueryOptions<FileMetadata> options)
        {
            try
            {
                var fileMetadata =
                    await _mediator.Send(new GetFileMetadata(key), _applicationLifetime.ApplicationStopping);
                if (fileMetadata == null) return NotFound();

                return Ok(options.ApplyTo(new[] { fileMetadata }.AsQueryable()).SingleOrDefault());
            }
            catch (InvalidRequestException ex)
            {
                _logger.LogWarning(ex, string.Format(Resources.Resources.Invalid_request, "GET entity by key"));
                return new BadRequestObjectResult(Resources.Resources.Key_validation_message);
            }
            catch (ODataException ex)
            {
                _logger.LogError(ex, Resources.Resources.OData_error);
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, Resources.Resources.General_error_get_request);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Allows querying files metadata by specifying file type and name using OData standard
        /// </summary>
        /// <response code="200">`OK` <br/>The operation was successful. The response body includes requested file metadata</response>
        /// <response code="400">`Bad request` <br/>The request was invalid or not processable using OData standard</response>
        /// <response code="404">`Not found` <br/>There is no file metadata which meets given criteria.</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="type">Type of the file that is queried</param>
        /// <param name="name">Name of the file that is queried</param>
        /// <param name="options">The current OData query options.</param>

        /// <returns>Metadata entity with requested properties</returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(FileMetadata), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ByTypeByName(string type, string name, ODataQueryOptions<FileMetadata> options)
        {
            try
            {
                var fileMetadata = await _mediator.Send(new GetFileMetadata(type, name), _applicationLifetime.ApplicationStopping);
                if (fileMetadata == null) return NotFound();

                return Ok(options.ApplyTo(new[] { fileMetadata }.AsQueryable()).SingleOrDefault());
            }
            catch (ODataException ex)
            {
                _logger.LogError(ex, Resources.Resources.OData_error);
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, Resources.Resources.General_error_get_request);
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Creates metadata entity using properties send in request body
        /// </summary>
        /// <response code="201">`Created` <br/>The operation was successful. The response body includes url to the newly created entity and its metadata</response>
        /// <response code="400">`Bad Request` <br/>Sent request was invalid</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="fileMetadata">Metadata entity with defined and custom properties</param>
        /// <returns>Uri to the newly created entity and its metadata</returns>
        [HttpPost]
        [ProducesResponseType(typeof(FileMetadata), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> PostAsync([FromBody] UploadFileRequest fileMetadata)
        {
            try
            {
                if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);

                return Created(new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}/{fileMetadata.Name}", UriKind.Absolute),
                        await _mediator.Send(fileMetadata, _applicationLifetime.ApplicationStopping));
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Creates or updates metadata entity using properties send in request body
        /// </summary>
        /// <response code="202">`Accepted` <br/>The operation was successful. The response body includes url to the created or updated entity and its metadata</response>
        /// <response code="400">`Bad Request` <br/>Sent request was invalid</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="fileMetadata">Metadata entity with defined and custom properties</param>
        /// <returns>Uri to the created or updated entity and its metadata</returns>
        [HttpPut]
        [ProducesResponseType(typeof(FileMetadata), (int)HttpStatusCode.Accepted)]
        public async Task<IActionResult> PutAsync([FromBody] UpdateFileMetadata fileMetadata)
        {
            try
            {
                if (!ModelState.IsValid) return new BadRequestObjectResult(ModelState);
                return Accepted(new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}/{fileMetadata.Name}", UriKind.Absolute),
                        await _mediator.Send(fileMetadata, _applicationLifetime.ApplicationStopping));
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Removes metadata entity using given partition and row keys
        /// </summary>
        /// <response code="204">`No Content` <br/>The operation was successful. Entity was successfully deleted</response>
        /// <response code="400">`Bad request` <br/>The request was invalid or not processable using OData standard</response>
        /// <response code="404">`Not Found` <br/>Requested entity was not found</response>
        /// <response code="500">`Internal Server Error` <br/>The service is unable to fulfill the request at this time.</response>
        /// <param name="key">Key is a combination of type(partition key) and name (row key) separated by dash i.e. voice_announcement-zone_one_english</param>
        /// <returns>No content when entity successfully deleted, otherwise not found</returns>
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteAsync(string key)
        {
            try
            {
                var result = await _mediator.Send(new DeleteFileRequest(key), _applicationLifetime.ApplicationStopping);
                return result ? (IActionResult)NoContent() : NotFound();
            }
            catch (InvalidRequestException ex)
            {
                _logger.LogWarning(ex, string.Format(Resources.Resources.Invalid_request, "GET entity by key"));
                return new BadRequestObjectResult(Resources.Resources.Key_validation_message);
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
