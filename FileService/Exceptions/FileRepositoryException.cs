﻿using System;
using System.Runtime.Serialization;

namespace FileService.Exceptions
{
    public class FileRepositoryException : Exception
    {
        public FileRepositoryException()
            : base()
        {
        }

        public FileRepositoryException(string message)
            : base(message)
        {
        }

        public FileRepositoryException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected FileRepositoryException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
