﻿using System;
using System.Runtime.Serialization;

namespace FileService.Exceptions
{
    [Serializable]
    public class FileStorageException : Exception
    {
        public int Code { get; set; }

        public FileStorageException()
        {
        }

        public FileStorageException(string message) : base(message)
        {
        }

        public FileStorageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public FileStorageException(string message, Exception innerException, int httpCode) : base(message, innerException)
        {
            Code = httpCode;
        }

        protected FileStorageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
