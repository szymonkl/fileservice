﻿using FileService.Application.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNetCore.Mvc;

namespace FileService.Infrastructure
{
    public class FileMetadataConfiguration : IModelConfiguration
    {
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion, string routePrefix)
        {
            var fileType = builder.EntityType<FileMetadata>();
            fileType.HasKey(f => f.Name);

            var filesByTypeByName = builder.EntityType<FileMetadata>().Collection
                .Function("ByTypeByName")
                .ReturnsCollectionFromEntitySet<FileMetadata>("Files");
            filesByTypeByName.Parameter<string>("type").Required();
            filesByTypeByName.Parameter<string>("name").Required();

            builder.EntitySet<FileMetadata>("Files");
        }
    }
}
