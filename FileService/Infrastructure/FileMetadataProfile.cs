﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Tables;

namespace FileService.Infrastructure
{
    public class FileMetadataProfile : Profile
    {
        public FileMetadataProfile()
        {
            CreateMap<FileEntity, FileMetadata>()
                .ForMember(
                    dest => dest.CustomMetadata,
                    opt => opt.MapFrom(src => src.CustomMetadata))
                .ForMember(
                    dest => dest.ExistsOnBlob,
                    opt => opt.Ignore());
        }
    }
}
