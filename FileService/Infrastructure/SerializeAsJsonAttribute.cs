﻿using System;

namespace FileService.Infrastructure
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SerializeAsJsonAttribute : Attribute
    {
    }
}