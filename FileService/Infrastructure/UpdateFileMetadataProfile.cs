﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Tables;
using System;

namespace FileService.Infrastructure
{
    public class UpdateFileMetadataProfile : Profile
    {
        public UpdateFileMetadataProfile()
        {
            CreateMap<UpdateFileMetadata, FileEntity>()
                .ForMember(
                    dest => dest.PartitionKey,
                    opt => opt.MapFrom(src => src.Type))
                .ForMember(
                    dest => dest.RowKey,
                    opt => opt.MapFrom(src => src.Name))
                .ForMember(
                    dest => dest.Timestamp,
                    opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(
                    dest => dest.UploadTime,
                    opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(
                    dest => dest.Url,
                    opt => opt.Ignore())
                .ForMember(
                    dest => dest.ETag,
                    opt => opt.Ignore());
        }
    }
}
