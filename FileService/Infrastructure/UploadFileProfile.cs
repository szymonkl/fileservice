﻿using AutoMapper;
using FileService.Application.Models;
using FileService.Persistence.Tables;
using System;
using System.IO;

namespace FileService.Infrastructure
{
    public class UploadFileProfile : Profile
    {
        public UploadFileProfile()
        {
            CreateMap<UploadFileRequest, FileEntity>()
                .ForMember(
                    dest => dest.PartitionKey,
                    opt => opt.MapFrom(src => src.Type))
                .ForMember(
                    dest => dest.RowKey,
                    opt => opt.MapFrom(src => src.Name))
                .ForMember(
                    dest => dest.UploadTime,
                    opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(
                    dest => dest.Timestamp,
                    opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(
                    dest => dest.Extension,
                    opt => opt.MapFrom(src => Path.GetExtension(src.LocalPath)))
                .ForMember(
                    dest => dest.UploadingStatus,
                    opt => opt.Ignore())
                .ForMember(
                    dest => dest.Url,
                    opt => opt.Ignore())
                .ForMember(
                    dest => dest.ETag,
                    opt => opt.Ignore());
        }
    }
}
