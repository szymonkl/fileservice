﻿using FileService.Exceptions;
using FileService.Utilities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.WindowsAzure.Storage;
using System;

namespace FileService.Persistence
{
    public class AzureStorageAccountProvider : IAzureStorageAccountProvider
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;

        public AzureStorageAccountProvider(
            IConfiguration configuration,
            IWebHostEnvironment hostingEnvironment)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _environment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }

        public CloudStorageAccount GetAccount()
        {
            var azureStorageConnectionString = _configuration.GetConnectionString(Constants.AzureStorageConnectionString);

            if (string.IsNullOrEmpty(azureStorageConnectionString))
            {
                if (_environment.IsDevelopment())
                {
                    return CloudStorageAccount.DevelopmentStorageAccount;
                }

                throw new InvalidConfigurationException(Resources.Resources.Unable_to_retrieve_storage_connection_string);
            }

            if (!CloudStorageAccount.TryParse(azureStorageConnectionString, out CloudStorageAccount storageAccount))
            {
                throw new InvalidConfigurationException(Resources.Resources.Unable_to_parse_storage_connection_string);
            }
            return storageAccount;
        }
    }
}