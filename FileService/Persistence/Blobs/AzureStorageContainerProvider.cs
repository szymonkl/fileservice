﻿using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace FileService.Persistence.Blobs
{
    public class AzureStorageContainerProvider : IAzureStorageContainerProvider
    {
        private readonly IAzureStorageAccountProvider _accountProvider;
        private readonly ILogger<AzureStorageContainerProvider> _logger;

        public AzureStorageContainerProvider(IAzureStorageAccountProvider accountProvider, ILogger<AzureStorageContainerProvider> logger)
        {
            _accountProvider = accountProvider ?? throw new ArgumentNullException(nameof(accountProvider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<CloudBlobContainer> ConfigureBlobContainer(string containerName)
        {
            try
            {
                var storageAccount = _accountProvider.GetAccount();
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                var blobContainer = cloudBlobClient.GetContainerReference(containerName);

                bool created = await blobContainer.CreateIfNotExistsAsync();
                _logger.LogInformation(created
                    ? string.Format(Resources.Resources.Blob_container_created, containerName)
                    : string.Format(Resources.Resources.Blob_container_exists, containerName));

                return blobContainer;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Blob_container_creation_failed, containerName));
                throw;
            }
        }

        public Uri Endpoint => _accountProvider.GetAccount().BlobEndpoint;
    }
}
