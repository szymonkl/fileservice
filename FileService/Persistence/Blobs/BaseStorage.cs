﻿using FileService.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileService.Persistence.Blobs
{
    public class BaseStorage : IBaseStorage
    {
        private readonly IAzureStorageContainerProvider _containerProvider;
        private readonly string _containerName;
        private readonly ILogger<BaseStorage> _logger;

        public BaseStorage(IAzureStorageContainerProvider containerProvider, string containerName, ILogger<BaseStorage> logger)
        {
            _containerProvider = containerProvider ?? throw new ArgumentNullException(nameof(containerProvider));
            _containerName = containerName ?? throw new ArgumentNullException(nameof(containerName));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<bool> DoesFileExistsAsync(string blobName)
        {
            var blobDir = await GetBlockBlobAsync(blobName);

            return await blobDir.ExistsAsync();
        }

        public async Task<IEnumerable<string>> GetAllFileUrls(string fileExtension = null)
        {
            try
            {
                var blobContainer = await GetCloudBlobContainerAsync(_containerName);

                var blobFiles = new List<IListBlobItem>();
                // List folders
                var blobDirectories = new List<IListBlobItem>();
                BlobContinuationToken continuationTokenDirs = null;

                do
                {
                    var directoriesResult = await blobContainer.ListBlobsSegmentedAsync(continuationTokenDirs);
                    continuationTokenDirs = directoriesResult.ContinuationToken;

                    // Add files without directories
                    blobFiles.AddRange(directoriesResult.Results.OfType<CloudBlockBlob>());
                    blobDirectories.AddRange(directoriesResult.Results.OfType<CloudBlobDirectory>());
                }
                while (continuationTokenDirs != null);

                _logger.LogInformation(string.Format(Resources.Resources.Found_blob_directories, blobDirectories.Count, blobContainer.Name));

                // List files in folders
                foreach (var directory in blobDirectories)
                {
                    BlobContinuationToken continuationTokenFiles = null;
                    do
                    {
                        var fileResult = await blobContainer.ListBlobsSegmentedAsync(continuationTokenFiles);
                        continuationTokenFiles = fileResult.ContinuationToken;
                        blobFiles.AddRange(fileResult.Results.OfType<CloudBlockBlob>());
                    }
                    while (continuationTokenFiles != null);

                    _logger.LogInformation(string.Format(Resources.Resources.Found_blob_files, blobFiles.Count, directory));
                }

                if (fileExtension != null)
                {
                    blobFiles = blobFiles.Where(x => ((CloudBlockBlob)x).Properties.ContentType.Equals(fileExtension)).ToList();
                }

                return blobFiles.Select(i => i.Uri.AbsoluteUri);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Blob_exception, _containerName));

                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(GetAllFileUrls)), ex);
            }
        }

        public async Task<string> GetFileUrlAsync(string blobName,
            SharedAccessBlobPermissions permissions = SharedAccessBlobPermissions.None | SharedAccessBlobPermissions.Read |
                                                      SharedAccessBlobPermissions.List, double minutesToExpire = 5)
        {
            try
            {
                var blob = await GetBlockBlobAsync(blobName);
                var sasConstraints = new SharedAccessBlobPolicy()
                {
                    SharedAccessStartTime = DateTimeOffset.UtcNow.AddMinutes(-5),
                    SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddMinutes(minutesToExpire),
                    Permissions = permissions,
                };

                //Generate the shared access signature on the blob, setting the constraints directly on the signature.
                var sasBlobToken = blob.GetSharedAccessSignature(sasConstraints);

                var blobUri = blob.Uri.AbsoluteUri;
                var uploadUrl = $"{blobUri}{sasBlobToken}";

                return uploadUrl;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Blob_exception, blobName));

                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(GetAllFileUrls)), ex);
            }
        }

        public async Task UploadFileAsync(string localPath, string fileName)
        {
            try
            {
                var blob = await GetBlockBlobAsync(fileName);

                await blob.UploadFromFileAsync(localPath);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Exception_in_for, nameof(UploadFileAsync), fileName));

                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(UploadFileAsync)), ex);
            }
        }

        public async Task DownloadFileAsync(string fileName, string blobName)
        {
            try
            {
                var blob = await GetBlockBlobAsync(blobName);

                await blob.DownloadToFileAsync(fileName, FileMode.Create);
            }
            catch (StorageException ex)
            {
                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(DownloadFileAsync), ex, ex.RequestInformation.HttpStatusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Exception_in_for, nameof(DownloadFileAsync), blobName));

                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(DownloadFileAsync), ex));
            }
        }

        public async Task RemoveFileAsync(string blobName)
        {
            try
            {
                var container = await GetCloudBlobContainerAsync(_containerName);

                if (!await DoesFileExistsAsync(blobName))
                    return;
                var blob = container.GetBlockBlobReference(blobName);
                await blob.DeleteAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Exception_in_for, nameof(RemoveFileAsync), blobName));

                throw new FileStorageException(string.Format(Resources.Resources.Exception_in, nameof(RemoveFileAsync)), ex);
            }
        }

        private async Task<CloudBlockBlob> GetBlockBlobAsync(string blobName)
        {
            var container = await GetCloudBlobContainerAsync(_containerName);

            return container.GetBlockBlobReference(blobName);
        }

        private async Task<CloudBlobContainer> GetCloudBlobContainerAsync(string containerName)
        {
            return await _containerProvider.ConfigureBlobContainer(containerName);
        }
    }
}
