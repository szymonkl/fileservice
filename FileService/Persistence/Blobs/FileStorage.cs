﻿using Microsoft.Extensions.Logging;

namespace FileService.Persistence.Blobs
{
    public class FileStorage : BaseStorage, IFileStorage
    {
        public FileStorage(IAzureStorageContainerProvider containerProvider, string containerName, ILogger<BaseStorage> logger) : base(containerProvider, "filestorage", logger)
        {
            // For testing purposes
        }
        public FileStorage(IAzureStorageContainerProvider containerProvider, ILogger<BaseStorage> logger) : base(containerProvider, "filestorage", logger)
        {
            // For application purposes
        }
    }
}
