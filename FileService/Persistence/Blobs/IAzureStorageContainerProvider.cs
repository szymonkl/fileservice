﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Threading.Tasks;

namespace FileService.Persistence.Blobs
{
    public interface IAzureStorageContainerProvider
    {
        Task<CloudBlobContainer> ConfigureBlobContainer(string containerName);

        Uri Endpoint { get; }
    }
}
