﻿using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileService.Persistence.Blobs
{
    public interface IBaseStorage
    {
        Task<bool> DoesFileExistsAsync(string blobName);
        Task<IEnumerable<string>> GetAllFileUrls(string fileExtension = null);
        Task<string> GetFileUrlAsync(string blobName, SharedAccessBlobPermissions permissions = SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List, double minutesToExpire = 5);
        Task UploadFileAsync(string localPath, string fileName);
        Task DownloadFileAsync(string fileName, string blobName);
        Task RemoveFileAsync(string blobName);
    }
}
