﻿using Microsoft.WindowsAzure.Storage;

namespace FileService.Persistence
{
    public interface IAzureStorageAccountProvider
    {
        CloudStorageAccount GetAccount();
    }
}