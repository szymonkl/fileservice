﻿using FileService.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Persistence.Tables
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : TableEntity, new()
    {
        public Task Initialization { get; }

        private readonly ILogger<BaseRepository<TEntity>> _logger;
        private CloudTable _table;

        protected BaseRepository(ICloudTableProvider tableProvider, string tableName, ILogger<BaseRepository<TEntity>> logger)
        {
            if (tableProvider == null) throw new ArgumentNullException(nameof(tableProvider));
            if (string.IsNullOrEmpty(tableName)) throw new ArgumentNullException(nameof(tableName));

            _logger = logger;
            Initialization = InitializeAsync(tableProvider, tableName);
        }

        private async Task InitializeAsync(ICloudTableProvider tableProvider, string tableName)
        {
            if (await tableProvider.TryConfigureStorageTable(tableName, out _table))
            {
                _logger.LogInformation(string.Format(Resources.Resources.Configured_table, tableName));
            }
            else
            {
                _logger.LogError(string.Format(Resources.Resources.Could_not_configure_table, tableName));
                throw new FileRepositoryException(string.Format(Resources.Resources.Could_not_configure_table, tableName));
            }
        }

        public CloudTable Table => _table ?? throw new ArgumentNullException(Resources.Resources.Could_not_configure_table);

        public virtual async Task AddEntity(TEntity entity)
        {
            try
            {
                await Initialization;

                _logger.LogInformation(string.Format(Resources.Resources.Inserting_entity, entity.RowKey));

                var insertOperation = TableOperation.Insert(entity);
                await Table.ExecuteAsync(insertOperation);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new FileRepositoryException(Resources.Resources.Inserting_entity, ex);
            }
        }

        public virtual async Task Delete(TEntity entity)
        {
            try
            {
                await Initialization;

                _logger.LogInformation(string.Format(Resources.Resources.Deleting_entity, entity.RowKey));

                var deleteOperation = TableOperation.Delete(entity);
                await Table.ExecuteAsync(deleteOperation);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new FileRepositoryException(Resources.Resources.Error_deleting_entity, ex);
            }
        }

        public virtual async Task Edit(TEntity entity)
        {
            try
            {
                await Initialization;

                _logger.LogInformation(string.Format(Resources.Resources.Editing_entity, entity.RowKey));

                var insertOperation = TableOperation.InsertOrMerge(entity);
                await Table.ExecuteAsync(insertOperation);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new FileRepositoryException(Resources.Resources.Error_updating_entity, ex);
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll(CancellationToken? cancellationToken)
        {
            try
            {
                await Initialization;
                var tableQuery = new TableQuery<TEntity>();
                var list = new List<TEntity>();

                TableContinuationToken continuationToken = null;

                _logger.LogInformation(Resources.Resources.Getting_all_entities);

                do
                {
                    // Retrieve a segment (up to 1,000 entities).
                    TableQuerySegment<TEntity> tableQueryResult =
                        await Table.ExecuteQuerySegmentedAsync(tableQuery, continuationToken, null, null, cancellationToken ?? CancellationToken.None);

                    // Assign the new continuation token to tell the service where to
                    // continue on the next iteration (or null if it has reached the end).
                    continuationToken = tableQueryResult.ContinuationToken;
                    list.AddRange(tableQueryResult.Results);
                    // Loop until a null continuation token is received, indicating the end of the table.
                } while (continuationToken != null);

                return list;
            }
            catch (OperationCanceledException ex)
            {
                _logger.LogWarning(string.Format(Resources.Resources.Operation_cancelled, ex.TargetSite));
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new FileRepositoryException(Resources.Resources.Error_gettiing_all_entities, ex);
            }
        }

        public virtual async Task<TEntity> Get(string partitionKey, string rowKey)
        {
            try
            {
                await Initialization;

                _logger.LogInformation(string.Format(Resources.Resources.Getting_entity, rowKey));

                var retrieveOperation = TableOperation.Retrieve<TEntity>(partitionKey, rowKey);
                var result = await Table.ExecuteAsync(retrieveOperation).ConfigureAwait(false);

                return result.Result as TEntity;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw new FileRepositoryException(Resources.Resources.Error_getting_entity, ex);
            }
        }
    }
}
