﻿using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Threading.Tasks;

namespace FileService.Persistence.Tables
{
    public class CloudTableProvider : ICloudTableProvider
    {
        private readonly IAzureStorageAccountProvider _storageAccountProvider;
        private readonly ILogger<CloudTableProvider> _logger;

        public CloudTableProvider(IAzureStorageAccountProvider storageAccountProvider, ILogger<CloudTableProvider> logger)
        {
            _storageAccountProvider = storageAccountProvider ?? throw new ArgumentNullException(nameof(storageAccountProvider));
            _logger = logger;
        }

        public Task<bool> TryConfigureStorageTable(string tableName, out CloudTable cloudTable)
        {
            var storageAccount = _storageAccountProvider.GetAccount();
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            cloudTable = tableClient.GetTableReference(tableName);
            return TableExistsAsync(cloudTable);
        }

        private async Task<bool> TableExistsAsync(CloudTable cloudTable)
        {
            try
            {
                if (await cloudTable.CreateIfNotExistsAsync())
                {
                    _logger.LogInformation(string.Format(Resources.Resources.Table_not_exists_creating, cloudTable.Name));
                }
                else
                {
                    _logger.LogInformation(string.Format(Resources.Resources.Table_exists, cloudTable.Name));
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format(Resources.Resources.Table_creating_failed, cloudTable.Name));
                return false;
            }
        }

        public Uri Endpoint => _storageAccountProvider.GetAccount().TableEndpoint;
    }
}