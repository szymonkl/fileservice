﻿using FileService.Infrastructure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;

namespace FileService.Persistence.Tables
{
    public class FileEntity : TableEntity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Target { get; set; }
        public string Url { get; set; }
        public string Extension { get; set; }
        public DateTime UploadTime { get; set; }
        public int UploadingStatus { get; set; }

        [SerializeAsJson]
        public Dictionary<string, object> CustomMetadata { get; set; }

        public override IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            var results = base.WriteEntity(operationContext);
            EntityJsonPropertyConverter.Serialize(this, results);
            return results;
        }

        public override void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            base.ReadEntity(properties, operationContext);
            EntityJsonPropertyConverter.Deserialize(this, properties);
        }
    }
}
