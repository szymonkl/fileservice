﻿using Microsoft.Extensions.Logging;

namespace FileService.Persistence.Tables
{
    public class FileRepository : BaseRepository<FileEntity>, IFileRepository
    {
        public FileRepository(ICloudTableProvider provider, string tableName, ILogger<BaseRepository<FileEntity>> logger) : base(provider, tableName, logger)
        {
        }
        public FileRepository(ICloudTableProvider provider, ILogger<BaseRepository<FileEntity>> logger) : base(provider, "filesMetadata", logger)
        {
        }
    }
}
