﻿using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FileService.Persistence.Tables
{
    public interface IBaseRepository<TEntity> where TEntity : TableEntity, new()
    {
        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="entity">Table entity to be created</param>
        /// <returns>Add entity task</returns>
        Task AddEntity(TEntity entity);

        /// <summary>
        /// Deletes specified entity
        /// </summary>
        /// <param name="entity">Table entity to be deleted</param>
        /// <returns>Delete entity task</returns>
        Task Delete(TEntity entity);

        /// <summary>
        /// Creates or updates specified entity
        /// </summary>
        /// <param name="entity">Entity to be created or updated</param>
        /// <returns>Edit entity task</returns>
        Task Edit(TEntity entity);

        /// <summary>
        /// Retrieves up to 1000 entities
        /// </summary>
        /// <returns>Get all entities task</returns>
        Task<IEnumerable<TEntity>> GetAll(CancellationToken? cancellationToken);

        /// <summary>
        /// Retrieves entity by specified partition and row keys
        /// </summary>
        /// <param name="partitionKey">Partition key to be queried by</param>
        /// <param name="rowKey">Row key to be queried by</param>
        /// <returns>Get entity task</returns>
        Task<TEntity> Get(string partitionKey, string rowKey);
    }
}