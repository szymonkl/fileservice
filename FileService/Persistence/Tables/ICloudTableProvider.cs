﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Threading.Tasks;

namespace FileService.Persistence.Tables
{
    public interface ICloudTableProvider
    {
        Task<bool> TryConfigureStorageTable(string tableName, out CloudTable cloudTable);
        Uri Endpoint { get; }
    }
}