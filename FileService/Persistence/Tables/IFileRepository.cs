﻿namespace FileService.Persistence.Tables
{
    public interface IFileRepository : IBaseRepository<FileEntity>
    {
    }
}
