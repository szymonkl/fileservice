using AutoMapper;
using FileService.Application.Validations;
using FileService.Controllers;
using FileService.Documentation;
using FileService.Persistence;
using FileService.Persistence.Blobs;
using FileService.Persistence.Tables;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Reflection;
using static Microsoft.AspNet.OData.Query.AllowedQueryOptions;

namespace FileService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            services.AddControllers().AddNewtonsoftJson().AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<UploadFileRequestValidator>();
                fv.RegisterValidatorsFromAssemblyContaining<UpdateFileMetadataValidator>();
            });
            services.AddApiVersioning(options => options.ReportApiVersions = true);
            services.AddOData().EnableApiVersioning();
            services.AddODataApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major[.minor][-status]"
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;

                    // configure query options (which cannot otherwise be configured by OData conventions)
                    options.QueryOptions.Controller<FilesController>()
                        .Action(c => c.GetAsync(default))
                        .Allow(Select | Filter | Count | OrderBy).AllowTop(100);

                    options.QueryOptions.Controller<FilesController>()
                        .Action(c => c.GetAsync(default, default))
                        .Allow(Select);

                    options.QueryOptions.Controller<FilesController>()
                        .Action(c => c.ByTypeByName(default, default, default))
                        .Allow(Select);
                });

            services.AddMediatR(typeof(Startup));
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            services.AddSwaggerGen(config =>
            {
                config.OperationFilter<SwaggerDefaultValues>();
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                config.IncludeXmlComments(xmlPath);
            });

            services.AddSingleton<IAzureStorageAccountProvider, AzureStorageAccountProvider>();
            services.AddScoped<ICloudTableProvider, CloudTableProvider>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IAzureStorageContainerProvider, AzureStorageContainerProvider>();
            services.AddScoped<IFileStorage, FileStorage>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, VersionedODataModelBuilder modelBuilder,
            IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.Count();
                endpoints.MapVersionedODataRoute("odata", "api", modelBuilder);
            });

            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }
                });
        }
    }
}
